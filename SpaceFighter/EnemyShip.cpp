
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) // checks for a delay
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); //checks the time elapsed against it
		 
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate(); // if the delay is over the object is now active
		}
	}

	if (IsActive()) //works if it made it past the previous function
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); 
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
		//now if the object had overstayed it's welcomeit will be booted out
	}

	Ship::Update(pGameTime); // all of the normal updates
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}